""" An example highlighting the difference between DMD and streaming DMD

    Streaming DMD is a modification of the "standard" DMD procedure that
    produces *APPROXIMATIONS* of the DMD modes and eigenvalues.  The benefit
    of this procedure is that it can be applied to data sets with large
    (in theory, infinite) numbers of snapshots provided the underlying
    system is effectively low-rank.

    Returns
    -------
    Outputs a plot comparing the streaming and standard eigenvalues
"""

import sys
sys.path.append('..')

from dmdtools.batch import DMD
from dmdtools.streaming import StreamingDMD
import numpy as np
import matplotlib.pyplot as plt

n_dynamic_modes = 4
max_rank = n_dynamic_modes   # maximum allowable rank of the DMD operator (0 = unlimited)
n_snaps = 501                   # total number of snapshots to be processed
n_states = 4000                 # number of states
noise_cov = 1.e-18               # measurement noise covariance

dt = 0.01                       # timestep
timesteps = np.linspace(0,n_snaps*dt,n_snaps)

np.random.seed(0)


def snapshots(n_states, n_snaps, noise_cov=0):
    # Define the example system
    V = np.random.randn(n_states,n_dynamic_modes)
    f = np.random.randint(1,10*n_dynamic_modes,n_dynamic_modes)

    for k in timesteps:
        x = 0
        for d in range(n_dynamic_modes):
        	x += V[:,d]*np.exp(1j*f[d]*k)
        yield x + np.sqrt(noise_cov) * np.random.randn(n_states)

def standard_dmd():
    X = np.zeros((n_states, n_snaps-1),dtype=np.complex_)
    Y = np.zeros((n_states, n_snaps-1),dtype=np.complex_)
    snaps = snapshots(n_states, n_snaps, noise_cov)
    x = next(snaps)
    for k, y in enumerate(snaps):
        X[:, k] = x
        Y[:, k] = y
        x = y

    DMD_model = DMD(n_rank=max_rank)
    DMD_model.fit(X, Y)
    return DMD_model.modes, DMD_model.evals


def streaming_dmd():
    sdmd = StreamingDMD(max_rank)
    snaps = snapshots(n_states, n_snaps, noise_cov)
    x = next(snaps)
    for y in snaps:
        sdmd.update(x, y)
        x = y
    return sdmd.compute_modes()


def main(streaming):
    modes, evals = streaming_dmd() if streaming else standard_dmd()
    fdmd = np.abs(np.angle(evals)) / (2 * np.pi * dt)
    n_modes = len(fdmd)
    ydmd = np.zeros(n_modes)
    for i in range(n_modes):
        ydmd[i] = np.linalg.norm(modes[:, i] * np.abs(evals[i]))
    ydmd /= max(ydmd)
    plt.stem(fdmd, ydmd)
    plt.show()


def compare_methods():

    np.random.seed(0)
    modes, evals = standard_dmd()

    np.random.seed(0)
    modes2, evals2 = streaming_dmd()

    evals.sort()
    evals2.sort()
    print("standard:")
    print(evals.imag/dt)
    print("\nstreaming:")
    print(evals2.imag/dt)
    plt.plot(evals.real, evals.imag, 'x')
    plt.plot(evals2.real, evals2.imag, '+')
    plt.legend(["DMD", "Streaming"])
    plt.title("DMD Spectrum")
    plt.xlabel(r"$\Re(\lambda)$")
    plt.ylabel(r"$\Im(\lambda)$")
    plt.show()

    print(np.allclose(evals, evals2))

if __name__ == "__main__":
    streaming = True
    #main(streaming)
    compare_methods()
