# Pythonisation of the StreamingTDMD matlab version (https://github.com/cwrowley/dmdtools/blob/master/matlab/StreamingTDMD.m) by Clancy Rowley et. al. Python implementation is based on StreamingDMD.py (https://github.com/cwrowley/dmdtools/blob/master/python/scripts/streaming_dmd_example.py) also from Clancy Rowley et. al.
# Brendan Michael 15/12/2020

import numpy as np

class StreamingTMD:
    def __init__(self, max_rank=None, ngram=5, epsilon=1.e-10):
        self.max_rank = max_rank
        self.count = 0
        self.ngram = ngram      # number of times to reapply Gram-Schmidt
        self.epsilon = epsilon  # tolerance for expanding the bases

    def update(self, x, y):
        """Update the DMD computation with a pair of snapshots

        Add a pair of snapshots (x,y) to the data ensemble.  Here, if the
        (discrete-time) dynamics are given by z(n+1) = f(z(n)), then (x,y)
        should be measurements corresponding to consecutive states
        z(n) and z(n+1)
        """
        
        self.count += 1
        # Construct augmented snapshot
        z = np.concatenate([x,y])
        if z.ndim==1:
            z=np.expand_dims(z,axis=1)
        normz = np.linalg.norm(z)
        n = x.shape[0]
        # process the first iterate
        if self.count == 1:
            # Construct bases
            self.Qz = z/normz + 0*1j
            self.Gz = normz**2 + 0*1j # Make sure Gz is complex
            return
        
        # ---- TLS Stage, Step 1 ----
        # classical Gram-Schmidt reorthonormalization
        ztilde = np.zeros((self.Qz.shape[1],1))
        ez = z + 0*1j
        for igram in range(self.ngram):
            dz = np.dot(self.Qz.conj().T,ez)          
            ztilde = ztilde+dz
            ez -=  np.dot(self.Qz,dz)
        
        # ---- TLS step 2 ----
        # check basis for z and expand, if necessary
        normez = np.linalg.norm(ez) 
        if normez / normz > self.epsilon:
            # update basis for x      
            self.Qz = np.hstack([self.Qz,(ez / normez)])
                        
            if self.Gz.ndim==0:
                self.Gz = np.vstack([np.hstack([self.Gz,0]),np.zeros((1,2))])
            else:     
                self.Gz = np.vstack([np.hstack([self.Gz,np.zeros((self.Gz.shape[0],1))]),np.zeros((1,self.Gz.shape[1]+1))])
            
        ztilde = np.dot(self.Qz.conj().T,z)
        self.Gz += ztilde*ztilde.conj().T
    
        # ---- TLS Stage, step 3 ----
        # check if POD compression is needed
        r0 = self.max_rank
        if r0:
            if self.Qz.shape[1] > r0:
                #print('Compressing bases!')
                evals, evecs = np.linalg.eig(self.Gz) 
                idz = np.argsort(evals)
                idz = idz[-1:-1-r0:-1]   # indices of largest r0 eigenvalues
                qz = evecs[:, idz]
                self.Qz = self.Qz * np.asmatrix(qz)
                self.Gz = np.asmatrix(np.diag(evals[idz]))

        
    def compute_modes(self):
        nn = int(self.Qz.shape[0]/2)
        Qx, Rx = np.linalg.qr(self.Qz[:nn, :])
        r0 = self.max_rank
        
        if r0:
            Qx = Qx[:,:r0]
            Rx = Rx[:r0,:]
        Gx = Rx*self.Gz*Rx.conj().T
        A = Qx.conj().T.dot(self.Qz[nn:, :]).dot(self.Gz).dot(self.Qz[:nn, :].conj().T).dot(Qx)
        Ktilde = A*np.linalg.pinv(Gx)
        evals, evecK  = np.linalg.eig(Ktilde)
        modes = Qx.dot(evecK)      
        return modes, evals
